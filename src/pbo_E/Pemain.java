package pbo_E;

import javax.swing.JFrame;

public class Pemain extends JFrame{
	String nama;
	int pos;
	
	public Pemain(String nama, int pos) {
		this.nama = nama;
		this.pos = pos;
	}
	
	public Pemain() {}
	
	public String getNama() {
		return nama;
	}
	public void setNama(String nama) {
		this.nama = nama;
	}
	public int getPos() {
		return pos;
	}
	public void setPos(int pos) {
		this.pos = pos;
	}
	
	
}
