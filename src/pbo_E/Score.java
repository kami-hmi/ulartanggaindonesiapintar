package pbo_E;

import javax.swing.*;

public class Score extends JPanel{
	public JTextField score;
	
	public Score(String name) {
		score = new JTextField(name);
		setVisible(true);
	}
	
	public Score() {
		score = new JTextField();
		setVisible(true);
	}
}
