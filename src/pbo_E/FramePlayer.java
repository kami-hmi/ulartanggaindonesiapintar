package pbo_E;

import java.awt.Color;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class FramePlayer extends Pemain{

	public JPanel contentPane, panelJudul; // membuat panel konten
	public JTextField player1Nama, player2Nama; // textbox input nama
	
	Pemain pemain1 = new Pemain();
	Pemain pemain2 = new Pemain();

	public JButton btnSelesai; // button untuk next
	
	public SnakeAndLadder frame; // main class

	public FramePlayer(SnakeAndLadder mf) {
		
		setTitle("Ular Tangga Indonesia Pintar");
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 300, 230);
		
		// buat panel 
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		contentPane.setBackground(Color.red);
		
		frame=mf;
		
		// buat button "Main!"
		btnSelesai = new JButton("Main!");
		btnSelesai.setFont(new Font("Tahoma", Font.ITALIC, 16));
		btnSelesai.setBounds(100, 140, 89, 23);
		btnSelesai.setBackground(Color.GREEN);
		contentPane.add(btnSelesai);
		
		btnSelesai.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			pemain1.setNama(player1Nama.getText());
			pemain2.setNama(player2Nama.getText());
			
			if(pemain1.getNama().isEmpty() || pemain2.getNama().isEmpty())
				JOptionPane.showMessageDialog(null, "Mohon isi semua nama Player!", null, JOptionPane.ERROR_MESSAGE);
			else
				{
				frame.sidePanel.player1.player_label.setText(pemain1.nama);
				frame.sidePanel.player2.player_label.setText(pemain2.nama);
				frame.name1 = pemain1.nama;
				frame.name2 = pemain2.nama;
				frame.pos1 = 1;
				frame.pos2 = 1;
				frame.sidePanel.setScore1(0);
				frame.sidePanel.setScore2(0);
				frame.currentPlayer =1;
				frame.N =2;
				
				
				frame.lblTurn.setText("Sekarang Giliran " + pemain1.nama);
				frame.lblMessage.setText("");
				frame.setVisible(true);				
				dispose();
				JOptionPane.showMessageDialog(null, "Cara memainkan game ini adalah dengan cara menekan tombol dice \n yang ada pada bagian bawah side panel \n\n Player yang pertama mencapai finish box akan memenangkan game ini!");
				JOptionPane.showMessageDialog(null, pemain1.nama +" Adalah Warna Merah" );
				JOptionPane.showMessageDialog(null, pemain2.nama +" Adalah Warna Hitam");
				frame.gridPanel.grid[1].setBackground(Color.BLACK);
				}
			}
		});
		
		JLabel label = new JLabel("Masukkan Nama Pemain", SwingConstants.CENTER);
		label.setFont(new Font("Tahoma", Font.BOLD, 16));
		label.setBounds(0, 10, 300, 30);
		contentPane.add(label);
		
		// menampilkan panel untuk isi nama players
		btnSelesai.setVisible(true);
		
		// buat label "Pemain 1"
		JLabel lblPlayer = new JLabel("Pemain 1");
		lblPlayer.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblPlayer.setBounds(70, 50, 67, 20);
		contentPane.add(lblPlayer);
		
		// buat label "Pemain 2"
		JLabel lblPlayer_1 = new JLabel("Pemain 2");
		lblPlayer_1.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblPlayer_1.setBounds(70, 80, 67, 20);
		contentPane.add(lblPlayer_1);
		
		// buat input nama player 
		player1Nama = new JTextField(10);
		player1Nama.setBounds(160, 50, 67, 20);
		player1Nama.setColumns(10);
		contentPane.add(player1Nama);
		
		// buat input nama player 2
		player2Nama = new JTextField(10);
		player2Nama.setBounds(160, 80, 67, 20);
		player2Nama.setColumns(10);
		contentPane.add(player2Nama);
		
		contentPane.setVisible(true); // menampilkan panel
		
		setLocationRelativeTo(null); // agar window berada ditengah
	}
}
