package pbo_E;

import javax.swing.JLabel;

public class PlayerLabel extends Score{
	public JLabel player_label;
	
	public PlayerLabel(String name) {
		player_label = new JLabel(name);
	}
	
	public PlayerLabel() {
		player_label = new JLabel();
	}
}
