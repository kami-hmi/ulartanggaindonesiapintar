package pbo_E;

import java.awt.Color;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.Random;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

public class SnakeAndLadder extends JFrame {

	public JPanel contentPane;
	private FramePlayer nameFrame;
	public static GridPanel gridPanel;
	public SidePanel sidePanel;
	public JButton btn;
	public JLabel lblMessage, lblTurn, lblGot;

	public static int snakes[][], ladders[][];

	public String name1, name2;
	public int N, currentPlayer;
	public int pos1 = 0, pos2 = 0;
	private static Random generator;
	public static int dimension = 10;
	public static int Nsnakes, Nladders;
	private int steps1 = 0, steps2 = 0;

	final static Color snakehead = Color.BLUE, snaketail = Color.cyan,
			ladderstart = Color.MAGENTA, ladderend = Color.PINK;
	static Color oddCell = Color.WHITE, evenCell = Color.WHITE;

	public SnakeAndLadder() {
		setResizable(false);
		setTitle("Ular Tangga Indonesia Pintar");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(800, 620);
		
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		generator = new Random();
		
		// button untuk buat new game
		btn = new JButton("New Game");
		btn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				reinitialise();
			}
		});
		btn.setFont(new Font("Times New Roman", Font.BOLD | Font.ITALIC, 20));
		btn.setBounds(585, 500, 128, 33);
		contentPane.add(btn);
		
		// button untuk roll dice
		btn = new JButton("Roll Dice");
		btn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				diceRolled();
			}
		});
		btn.setFont(new Font("Times New Roman", Font.BOLD | Font.ITALIC, 20));
		btn.setBounds(585, 450, 128, 33);
		contentPane.add(btn);
			
		gridPanel = new GridPanel(dimension);
		gridPanel.setBounds(0, 0, 508, 551);
		contentPane.add(gridPanel);

		sidePanel = new SidePanel();
		sidePanel.setBounds(528, 0, 225, 284);
		contentPane.add(sidePanel);

		lblMessage = new JLabel("Snake at Position 45 got you!!", JLabel.CENTER);
		lblMessage.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 12));
		lblMessage.setBounds(518, 295, 285, 33);
		contentPane.add(lblMessage);

		lblTurn = new JLabel("Player's Turn", JLabel.CENTER);
		lblTurn.setFont(new Font("Times New Roman", Font.BOLD, 16));
		lblTurn.setBounds(579, 364, 144, 29);
		contentPane.add(lblTurn);

		lblGot = new JLabel("", JLabel.CENTER);
		lblGot.setFont(new Font("Times New Roman", Font.BOLD | Font.ITALIC, 16));
		lblGot.setBounds(585, 425, 145, 17);
		contentPane.add(lblGot);

		setLocationRelativeTo(null);

		setVisible(false);
		nameFrame = new FramePlayer(this);
		nameFrame.setVisible(true);
	}

//	int flag = 0;
	public void diceRolled()
	{
		lblMessage.setText("");
		int total = dimension * dimension;
		int jump = generator.nextInt(6) + 1;// 1 to 6
		lblGot.setText("Nilai Dadu " + jump);

		if (currentPlayer == 1)
		{
			steps1++;
			sidePanel.setScore1(steps1);
			int newposition = pos1 + jump;
			
			setPositions();
			// if kalo player menang
			if (newposition == total)
			{
				JOptionPane.showMessageDialog(null, "Congratulations " + name1 + "!! You have won the game in  " + steps1 + " steps");
				btn.setEnabled(false);
			}
			
			newposition = cek(newposition, total);
			setPositions();
		
//			cekTangga(newposition, total);
//			setPositions();
//			if(flag == 0) {
//				newposition = 62;
//				flag = 1;
//			}
//			
			if (newposition > total) {
				lblMessage.setText("You need " + (total - pos1) + " to win!!");
				newposition = pos1;
			}
			
			gridPanel.grid[newposition].setBackground(Color.RED);
//			if(newposition != 62 && newposition != 7 && newposition != 9 && newposition != 59 && 
//					newposition != 37 && newposition != 11 && newposition != 27 && newposition != 74) {
//				if (newposition != pos1 && pos2 != pos1)// Get the original colour of the cell
//				{
//					if (pos1 % 2 == 1)
//						gridPanel.grid[pos1].setBackground(oddCell);
//					else
//						gridPanel.grid[pos1].setBackground(evenCell);
//				} 
//				
//				else if (N == 2 && pos2 == pos1)
//					gridPanel.grid[pos1].setBackground(Color.BLACK);
//			}
			
			if (newposition != pos1 && pos2 != pos1)// Get the original colour of the cell
			{
				if (pos1 % 2 == 1)
					gridPanel.grid[pos1].setBackground(oddCell);
				else
					gridPanel.grid[pos1].setBackground(evenCell);
			} 
			
			else if (N == 2 && pos2 == pos1)
				gridPanel.grid[pos1].setBackground(Color.BLACK);

			pos1 = newposition;
			lblTurn.setText("Sekarang Giliran " + name2);
			currentPlayer = 2;

		}

		else if (currentPlayer == 2) {
			steps2++;
			sidePanel.setScore2(steps2);
			setPositions();
			int newposition = pos2 + jump;
			if (newposition == total) {
				JOptionPane.showMessageDialog(null, "Congratulations " + name2
						+ "!! You have won the game in  " + steps2 + " steps");
				btn.setEnabled(false);
			}

			newposition = cek(newposition, total);
			setPositions();

		//	cekTangga(newposition, total);
		//	setPositions();

			if (newposition > total) {
				lblMessage.setText("You need " + (total - pos2) + " to win!!");
				newposition = pos2;
			}

			gridPanel.grid[newposition].setBackground(Color.BLACK);
//			if(newposition != 62 && newposition != 7 && newposition != 9 && newposition != 59 && 
//					newposition != 37 && newposition != 11 && newposition != 27 && newposition != 74) {
//				if ((newposition != pos2 && pos1 != pos2))// Get the original colour of
//					// the cell
//				{
//					//gridPanel.grid[pos2].setBackground(oddCell);
//						if (pos2 % 2 == 1)
//							gridPanel.grid[pos2].setBackground(oddCell);
//						else
//							gridPanel.grid[pos2].setBackground(evenCell);
//				} 
//				else if (N == 2 && pos1 == pos2)
//					gridPanel.grid[pos2].setBackground(Color.RED);
//			}
			if ((newposition != pos2 && pos1 != pos2))// Get the original colour of
												// the cell
			{
				//gridPanel.grid[pos2].setBackground(oddCell);
				if (pos2 % 2 == 1)
					gridPanel.grid[pos2].setBackground(oddCell);
				else
					gridPanel.grid[pos2].setBackground(evenCell);
			} 
			else if (N == 2 && pos1 == pos2)
				gridPanel.grid[pos2].setBackground(Color.RED);

			pos2 = newposition;

			
				lblTurn.setText("Sekarang Giliran " + name1);
				currentPlayer = 1;
			}
		}
	

	// GBS
	public void reinitialise() {
		// TODO Auto-generated method stub

		setVisible(false);
		nameFrame = new FramePlayer(this);
		nameFrame.setVisible(true);
		
		pos1=pos2=0;
		steps1=steps2=0;

		gridPanel.setVisible(false);
		gridPanel = new GridPanel(dimension);
		gridPanel.setBounds(0, 0, 508, 551);
		contentPane.add(gridPanel);

		setPositions();

		sidePanel.setVisible(false);
		sidePanel = new SidePanel();
		sidePanel.setBounds(528, 0, 225, 284);
		contentPane.add(sidePanel);

		lblMessage.setText("");
		lblTurn.setText("Giliran Player's 1");
		btn.setEnabled(true);

	}

	public static void setInitial() {
		// TODO Auto-generated method stub
		snakes = new int[144][2];// Maximum no. of snakes
		ladders = new int[144][2];// Maximum no. of ladders

		Nsnakes = 2;
		Nladders = 2;
		// Setting default position of snakes and ladders
		snakes[0][0] = 62;
		snakes[0][1] = 7;
		snakes[1][0] = 37;
		snakes[1][1] = 11;
//		snakes[2][0] = 67;
//		snakes[2][1] = 18;
//		snakes[3][0] = 99;
//		snakes[3][1] = 2;
		
		ladders[0][0] = 9;
		ladders[0][1] = 59;
		ladders[1][0] = 27;
		ladders[1][1] = 74;
		
		setPositions();
	}

	public static void setPositions() {

		for (int i = 0; i < Nsnakes; ++i) {
			gridPanel.grid[snakes[i][0]].setBackground(snakehead);
			gridPanel.grid[snakes[i][1]].setBackground(snaketail);

		}
		for (int i = 0; i < Nladders; ++i) {
			gridPanel.grid[ladders[i][0]].setBackground(ladderstart);
			gridPanel.grid[ladders[i][1]].setBackground(ladderend);

		}
	}

	


//kalo player nginjak ular
	int cek(int newposition, int total) {
		
		for (int i = 0; i < Nsnakes; ++i)
		{
			if (snakes[i][0] == newposition)
			{
				lblMessage.setText("Snake at board position " + newposition + " got you!!");
				newposition = snakes[i][1];
				break;
			}
			
		}
	//	return newposition;

	// kalo player nginjak tangga
//	int cekTangga(int newposition, int total) {
		// kalo player nginjak tangga
		for (int i = 0; i < Nladders; ++i) {
			if (ladders[i][0] == newposition) {
				lblMessage.setText("You are up through ladder at position " + newposition + " !");
				newposition = ladders[i][1];
			if (newposition == total)
				{// Ladder directly to win.
				JOptionPane.showMessageDialog(null, "Congratulations " + name1 + "!! You have won the game in  " + steps1 + " steps");
				btn.setEnabled(false);
			}
				break;
				
		}
	}
		return newposition;
}
}
	
	